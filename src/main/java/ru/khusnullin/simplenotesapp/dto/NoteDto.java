package ru.khusnullin.simplenotesapp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.khusnullin.simplenotesapp.models.Note;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class NoteDto {
    private Long id;
    private String title;
    private String text;
    private Long authorId;

    public static NoteDto from(Note note) {
        return NoteDto.builder()
                .id(note.getId())
                .title(note.getTitle())
                .text(note.getText())
                .authorId(note.getAuthor().getId())
                .build();
    }

    public static List<NoteDto> from(List<Note> notes) {
        return notes.stream().map(NoteDto::from).collect(Collectors.toList());
    }
}
