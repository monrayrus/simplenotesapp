package ru.khusnullin.simplenotesapp.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class SignUpForm {
    @NotEmpty
    @Size(max = 30)
    private String username;

    @Email
    private String email;

    @NotEmpty
    private String password;
}
