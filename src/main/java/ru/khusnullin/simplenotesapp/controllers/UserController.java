package ru.khusnullin.simplenotesapp.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.khusnullin.simplenotesapp.dto.SignUpForm;
import ru.khusnullin.simplenotesapp.dto.UserDto;
import ru.khusnullin.simplenotesapp.models.User;
import ru.khusnullin.simplenotesapp.service.UserService;

import javax.validation.Valid;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/signup")
public class UserController {

    private final UserService userService;

    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> signUp(@RequestBody @Valid SignUpForm form) {
        HttpHeaders headers = new HttpHeaders();
        User user;
        try {
            user = userService.signUp(form);
            return new ResponseEntity<>(UserDto.from(user), headers, HttpStatus.CREATED);
        } catch (DataIntegrityViolationException e) {
            log.info(e.getClass().getCanonicalName());
            return new ResponseEntity<>(new UserDto(), headers, HttpStatus.NOT_ACCEPTABLE);
        }
    }
}
