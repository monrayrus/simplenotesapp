package ru.khusnullin.simplenotesapp.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ru.khusnullin.simplenotesapp.dto.NoteDto;
import ru.khusnullin.simplenotesapp.interfaces.NotesRepository;
import ru.khusnullin.simplenotesapp.models.Note;
import ru.khusnullin.simplenotesapp.service.NoteService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/notes")
@RequiredArgsConstructor
public class NoteController {

    private final NoteService noteService;
    private final NotesRepository notesRepository;

    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<NoteDto> getNoteById(@PathVariable("id") Long noteId) {
        if (noteId == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        NoteDto noteDto = this.noteService.getNoteById(noteId);

        if (noteDto == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(noteDto, HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<NoteDto> addNote(@RequestBody @Valid NoteDto note, Authentication authentication) {
        HttpHeaders headers = new HttpHeaders();
        if (note == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        this.noteService.save(note, authentication);
        return new ResponseEntity<>(note, headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<NoteDto> updateNote(@PathVariable("id") Long id, @RequestBody @Valid NoteDto note, Authentication authentication) {
        HttpHeaders headers = new HttpHeaders();
        if (this.noteService.updateNote(id, note, authentication)) {
            return new ResponseEntity<>(note, headers, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(headers, HttpStatus.METHOD_NOT_ALLOWED);
        }
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Note> deleteNote(@PathVariable("id") Long noteId, Authentication authentication) {
        NoteDto note = this.noteService.getNoteById(noteId);
        if (note == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (noteService.deleteById(noteId, authentication)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<NoteDto>> getAllNotes() {
        List<NoteDto> notes = noteService.getAllNotes();
        if (notes.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(notes, HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<NoteDto>> getNotesByUser(@PathVariable Long id) {
        List<NoteDto> notes = noteService.getNotesByUserId(id);
        if (notes.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(notes, HttpStatus.OK);
    }

}
