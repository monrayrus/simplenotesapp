package ru.khusnullin.simplenotesapp.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.khusnullin.simplenotesapp.dto.NoteDto;
import ru.khusnullin.simplenotesapp.interfaces.UserRepository;
import ru.khusnullin.simplenotesapp.models.User;
import ru.khusnullin.simplenotesapp.service.NoteService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;


//Для разных проверок по ходу дела

@Slf4j
@RestController
@RequestMapping("/raw")
@RequiredArgsConstructor
public class RawController {
    private final UserRepository userRepository;
    private final NoteService noteService;
    @GetMapping("/user")
    public ResponseEntity<String> getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Optional<User> user = userRepository.findByToken(authentication.getName());
        User userok = user.get();
        String email = userok.getEmail();
        log.info(email);
        return new ResponseEntity<>(email, HttpStatus.OK);
    }

    @RequestMapping(value = "/author/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<NoteDto>> getByUser(@PathVariable Long id) {
        List<NoteDto> notes = noteService.getNotesByUserId(id);
        if (notes.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(notes, HttpStatus.OK);
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<NoteDto> updateNote(@PathVariable("id") Long id, @RequestBody @Valid NoteDto note, Authentication authentication)  {
        HttpHeaders headers = new HttpHeaders();
        if(this.noteService.updateNote(id, note, authentication)) {
            return new ResponseEntity<>(note, headers, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }
}
