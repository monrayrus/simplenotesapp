package ru.khusnullin.simplenotesapp.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "usr")
public class User implements Serializable {

    private static final long serialVersionUID = -6229003141976035135L;

    public enum Role {
        USER, ADMIN
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "user_id")
    private Long id;

    @Column(unique = true)
    private String username;

    @Enumerated(EnumType.STRING)
    private Role role;
    @Column(unique = true)
    private String email;
    private String password;

    //TODO: реализовать возможность наличия нескольких токенов у одного пользователя
    private String token;

    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL)
    private List<Note> notes;

}
