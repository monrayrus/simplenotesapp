package ru.khusnullin.simplenotesapp.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.khusnullin.simplenotesapp.dto.NoteDto;
import ru.khusnullin.simplenotesapp.interfaces.NotesRepository;
import ru.khusnullin.simplenotesapp.interfaces.UserRepository;
import ru.khusnullin.simplenotesapp.models.Note;
import ru.khusnullin.simplenotesapp.models.User;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class NoteService {

    private final NotesRepository notesRepository;
    private final UserRepository userRepository;

    public NoteDto save(NoteDto noteDto, Authentication authentication) {
        User user = userRepository.findByToken(authentication.getName()).get();
        log.info("In NoteService addNote {}, from {}", noteDto, user.getEmail());
        Note note = Note.builder().title(noteDto.getTitle()).text(noteDto.getText()).author(user).build();
        notesRepository.save(note);
        return NoteDto.from(note);
    }


    public List<NoteDto> getAllNotes() {
        log.info("In NoteService getAllNotes {}");
        return NoteDto.from(notesRepository.findAll());
    }

    public NoteDto getNoteById(Long id) {
        log.info("In NoteService getNoteById {}", id);
        return NoteDto.from(notesRepository.getById(id));
    }

    public boolean deleteById(Long id, Authentication authentication) {
        log.info("In NoteService deleteById {}", id);
        if(isAuthor(authentication, id)) {
            notesRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public boolean updateNote(Long id, NoteDto noteDto, Authentication authentication) {
        log.info("In NoteService updateNote by id {}", id);
        if (!notesRepository.existsById(id)) {
            try {
                save(noteDto, authentication);
                return true;
            } catch (DataIntegrityViolationException e) {
                log.warn(String.valueOf(e.getClass()));
                return false;
            }
        }
        if(isAuthor(authentication, id)) {
            Note noteToUpdate = notesRepository.getById(id);
            noteToUpdate.setTitle(noteDto.getTitle());
            noteToUpdate.setText(noteDto.getText());
            notesRepository.save(noteToUpdate);
            return true;
        }
        return false;
    }

    public List<NoteDto> getNotesByUserId(Long id) {
        log.info("In NoteService getNotesByUserId {}", id);
        Optional<User> user = userRepository.findById(id);
        if(user.isPresent()) {
            return NoteDto.from(notesRepository.getAllByAuthor(user.get()));
        } else {
            return NoteDto.from(Collections.emptyList());
        }
    }

    private boolean isAuthor(Authentication authentication, Long id) {
        User user = userRepository.findByToken(authentication.getName()).get();
        User user2 = notesRepository.getById(id).getAuthor();
        log.warn("User 1 {}, User 2 {}", user.getId(), user2.getId());
        return user.equals(user2);
    }


}
