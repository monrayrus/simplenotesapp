package ru.khusnullin.simplenotesapp.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.khusnullin.simplenotesapp.dto.SignUpForm;
import ru.khusnullin.simplenotesapp.interfaces.UserRepository;
import ru.khusnullin.simplenotesapp.models.User;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public User signUp(SignUpForm form) {
        User user = User.builder()
                .email(form.getEmail())
                .password(passwordEncoder.encode(form.getPassword()))
                .username(form.getUsername())
                .role(User.Role.USER)
                .build();
        log.info(user.getUsername());
        userRepository.save(user);
        return user;
    }
}
