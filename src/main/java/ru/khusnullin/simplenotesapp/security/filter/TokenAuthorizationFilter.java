package ru.khusnullin.simplenotesapp.security.filter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;
import ru.khusnullin.simplenotesapp.interfaces.UserRepository;
import ru.khusnullin.simplenotesapp.models.User;
import ru.khusnullin.simplenotesapp.security.config.SecurityConfiguration;
import ru.khusnullin.simplenotesapp.security.details.AccountUserDetails;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Optional;

@RequiredArgsConstructor
public class TokenAuthorizationFilter extends OncePerRequestFilter {

    public static final String TOKEN_HEADER = "Authorization";
    private final UserRepository userRepository;
    private final ObjectMapper objectMapper;


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (request.getRequestURI().equals((SecurityConfiguration.LOGIN_FILTER_PROCESSES_URL))) {
            filterChain.doFilter(request, response);
        } else {
            String tokenHeader = request.getHeader(TOKEN_HEADER);
            if (tokenHeader != null && tokenHeader.startsWith("Bearer ")) {
                String token = tokenHeader.substring("Bearer ".length());
                Optional<User> userOptional = userRepository.findByToken(token);

                if (userOptional.isPresent()) {
                    try {
                        DecodedJWT decodedJWT = JWT.require(Algorithm.HMAC256("simple_secret_key")).build().verify(token);
                        logger.info("User email " + decodedJWT.getClaim("email"));
                        AccountUserDetails userDetails = new AccountUserDetails(userOptional.get());
                        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(token, null, userDetails.getAuthorities());
                        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                        filterChain.doFilter(request, response);
                    } catch (JWTVerificationException e) {
                        logger.warn("Wrong token");
                    }
                } else {
                    logger.warn("Wrong token");
                    response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                    objectMapper.writeValue(response.getWriter(), Collections.singletonMap("error", "user not found with token"));
                }
            } else {
                logger.warn("Token is missing");
                filterChain.doFilter(request, response);
            }
        }
    }
}
