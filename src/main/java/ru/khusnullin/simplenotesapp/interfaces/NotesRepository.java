package ru.khusnullin.simplenotesapp.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.khusnullin.simplenotesapp.dto.NoteDto;
import ru.khusnullin.simplenotesapp.models.Note;
import ru.khusnullin.simplenotesapp.models.User;

import java.util.List;

public interface NotesRepository extends JpaRepository<Note, Long> {
    List<Note> getAllByAuthor(User user);
}
