package ru.khusnullin.simplenotesapp.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.khusnullin.simplenotesapp.models.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmail(String email);

    Optional<User> findByToken(String token);

    Optional<User> findById(Long id);
}
